// Preloader

setTimeout(() => {
    $(".body").css("opacity", "1");
}, 5);


$(window).on("load", function () {
    if (confirm("Открыть страницу с анимацией?")) {
        $(".preloader").fadeOut();
        $(".body").removeClass("body_fixed");
    } else {
        $(".queeze__form, .price__item, .advantages__item")
            .removeClass("animated-y-start animated animation");

        $(".banner__image-img")
            .removeClass("animated-x-start animated animation");
        $(".preloader").fadeOut();
        $(".body").removeClass("body_fixed");
    }
});

// Animation

$(".queeze__form, .price__item, .advantages__item")
    .addClass("animated-y-start animated animation");

$(".banner__image-img")
    .addClass("animated-x-start animated animation");

const animationEl = $(".animation");
const animatedClass = "animated";
const xStartAnimation = "animated-x-start";
const yStartAnimation = "animated-y-start";

function animateX() {
    animationEl.each(function (index, el) {
        const element = $(this);
        if (element.hasClass(xStartAnimation)) {
            element.removeClass(xStartAnimation);
            setTimeout(function () {
                element.removeClass(animatedClass);
            }, 800)
        }
    });
}

function animateY() {
    animationEl.each(function () {
        const element = $(this);
        const offSetTop = element.offset().top;
        const halfScreen = $(window).height() * 1.35;
        if (offSetTop <= halfScreen) {
            if (element.hasClass(yStartAnimation)) {
                element.removeClass(yStartAnimation);
                setTimeout(function () {
                    element.removeClass(animatedClass);
                }, 800)
            }
        }
    });
}

function animatePage() {
    if ($(window).width() > 992) {
        const offSetTop = animationEl.offset().top;
        if (offSetTop >= 0) {
            animateX();
        }
        $("body").scroll(function () {
            animateY();
        })
    }
}

$(window).on("load", function () {
    animatePage();
});


// Form validation: return number of invalid inputs

function checkFormValidation(inputs, notValidValues) {
    inputs.filter(function () {
        return !$(this).attr("id").includes(notValidValues)
    }).each(function (index, item) {
        if (!$(this).val().length) {
            $(this).addClass("input_error");
            $(this).closest(".input_wrapper").find(".input__error").removeAttr("hidden");
        } else {
            $(this).removeClass("input_error");
            $(this).closest(".input_wrapper").find(".input__error").attr("hidden", true);
        }
    });
    return inputs.filter(function () {
        return !$(this).attr("id").includes(notValidValues)
    }).filter(function (index, item) {
        return !$(this).val().length;
    }).length;
}

// Common functions

function submitForm(form, inputs, event) {
    event.preventDefault();
    if (!checkFormValidation(inputs, null)) {
        console.log(form.serialize());
        $("#thanksModal, .overlay").fadeIn(0);
        $("#modalForm").fadeOut(0);
        $("#modalFormMail").fadeOut(0);
        return form.serialize();
    }
}

// Inputs blur

$("input[type='text'], input[type='tel']").blur(function (event) {
    const inputValue = event.target.value;
    if (inputValue.length) {
        $(this).addClass("input_value");
    } else {
        $(this).removeClass("input_value");
    }
});

// Advantages slider

const advSlider = $(".advantages__row");

advSlider.on('init', (event, slick) => {
    $(".advantages__arrows-counter-common").text(slick.$slides.length);
});

if ($(window).innerWidth() <= 992) {
    advSlider.slick({
        arrows: false,
    });

    advSlider.on('afterChange', (event, slick, currentSlide) => {
        $(".advantages__arrows-counter-current").text(currentSlide + 1);
    });
}

$(".advantages__arrow-left").click(() => {
    advSlider.slick("slickPrev");
});

$(".advantages__arrow-right").click(() => {
    advSlider.slick("slickNext");
});

// Queeze slider

const queezeSlider = $(".queeze__form-content");

queezeSlider.slick({
    arrows: false,
    draggable: false
});

$(".queeze__form-next, .queeze__form-miss").click((event) => {
    event.preventDefault();
    queezeSlider.slick("slickNext");
});

queezeSlider.on("beforeChange", (event, slick, currentSlide) => {
    const value = currentSlide + 1;
    if (value === 6) {
        $(".queeze__form-content").hide(0);
        queezeSlider.slick("unslick");
        $(".queeze__form-progress-info").html("Отправить ответы");
        $(".queeze__form-controllers").fadeOut(0);
        $(".queeze__form-finish").fadeIn();
        $(".queeze__form-progress-current").css("width", "100%");
    }
});

queezeSlider.on('afterChange', (event, slick, currentSlide) => {
    const value = currentSlide + 1;
    $(".queeze__form-progress-current-index").text(value);
    if (value >= 6) {
        $(".queeze__form-progress-current").css("width", "100%");
    } else {
        $(".queeze__form-progress-current").css("width", `calc(100% / (6 / ${value}))`);
    }
    setTimeout(function () {
        if (value >= 4) {
            $(".queeze__form-progress-info").addClass("queeze__form-progress-info_white");
        } else {
            $(".queeze__form-progress-info").removeClass("queeze__form-progress-info_white");
        }
    }, 400);
    if (value > 6) {
        $(".queeze__form-progress-info").html("");
        $(".queeze__form-controllers").fadeOut(0);
        $(".queeze__form-finish").fadeIn();
    }
});

// File Attachment

const hiddenFileInput = $(".evaluation__form-hidden-file");
const fileTitle = $(".evaluation__form-file-title");

hiddenFileInput.change((event) => {
    const files = event.target.files;
    if (files.length) {
        fileTitle.text(files[0].name);
    } else {
        fileTitle.text("ПРИКРЕПИТЬ СМЕТУ");
    }
});

fileTitle.click(() => {
    hiddenFileInput.click();
});

// Evaluation Form

const evaluationForm = $("#evaluationForm");

const evaluationFormInput = $("#evaluationForm input");

evaluationForm.submit((event) => {
    submitForm(evaluationForm, evaluationFormInput, event);
});

// Queeze Form Submit

const queezeForm = $("#queezeForm");
const queezeFormInput = $("#queezeForm input:not([type='radio'])")

queezeForm.submit(function (event) {
    submitForm(queezeForm, queezeFormInput, event);
});

// Projects slider

const projectsImagesSlider = $(".project__slider-images");
const projectsItemsSlider = $(".projecs__slider-items");


projectsImagesSlider.on('init', (event, slick) => {
    $(".projects__slider-arrows-count-common").text(slick.$slides.length);
});

projectsImagesSlider.slick({
    items: 1,
    arrows: false,
    draggable: false,
});
projectsItemsSlider.slick({
    items: 1,
    arrows: false,
    draggable: false,
});


$(".projects__slider-arrows-prev").click(() => {
    projectsImagesSlider.slick("slickPrev");
    projectsItemsSlider.slick("slickPrev");
});

$(".projects__slider-arrows-next").click(() => {
    projectsImagesSlider.slick("slickNext");
    projectsItemsSlider.slick("slickNext");
});

projectsImagesSlider.on('afterChange', (event, slick, currentSlide) => {
    $(".projects__slider-arrows-count-current").text(currentSlide + 1);
});

// Questions Form

const questionsForm = $("#questionsForm");
const questionsFormInput = $("#questionsForm input");
const callbackType = $(".questions__form-type");

callbackType.click(function () {
    callbackType.removeClass("questions__form-type_active");
    $(this).addClass("questions__form-type_active");
    console.log($(this).attr("id"));
});

questionsForm.submit(function (event) {
    submitForm(questionsForm, questionsFormInput, event);
});


// Modal Form Submit

const modalForm = $("#modalForm");
const modalFormMail = $("#modalFormMail");
const modalFormInput = $("#modalForm input");
const modalFormMailInput = $("#modalFormMail input");

modalForm.submit(function (event) {
    submitForm(modalForm, modalFormInput, event);
});

modalFormMail.submit(function (event) {
    submitForm(modalFormMail, modalFormMailInput, event);
});

// Modal

$("button[data-target='modal']").click(function () {
    $(".overlay, #modalForm").fadeIn(200);
});

$("button[data-target='modalMail']").click(function () {
    $(".overlay, #modalFormMail").fadeIn(200);
});

$(".modal-form__close, .overlay").click((function () {
    modalFormInput.val("");
    modalFormInput.removeClass("input_value");
    modalFormInput.removeClass("input_error");
    modalFormInput.closest(".input_wrapper").find(".input__error").attr("hidden", true);
    modalFormMailInput.val("");
    modalFormMailInput.removeClass("input_value");
    modalFormMailInput.removeClass("input_error");
    modalFormMailInput.closest(".input_wrapper").find(".input__error").attr("hidden", true);
    $(".overlay, #modalForm").fadeOut(200);
    $(".overlay, #modalFormMail").fadeOut(200);
    $(".overlay, #thanksModal").fadeOut(200);
}));

// Houses modal

$(".house__close").click(function () {
    $(".house").removeClass("house_shown");
    $(".overlay").fadeOut(200);
});

$(".house__button").click(function () {
    $(".house").removeClass("house_shown");
    modalForm.fadeIn(200);
});

$(".price__item").click(function () {
    const targetId = $(this).attr("data-target");
    $(`${targetId}`).addClass("house_shown");
    $(".overlay").fadeIn(200);
});

$(".overlay").click(function () {
    $(".house").removeClass("house_shown");
});
